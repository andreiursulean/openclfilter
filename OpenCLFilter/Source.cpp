#include <CL/cl.hpp>
#include <string>
#include <iostream>
#include <chrono>
#include <cstdio>

struct tjregion {
	int x;
	int y;
	int w;
	int h;
};

struct tjtransform {
	void* data;
};

struct StegoData {
	char* message;
	bool written;
	int writtenChars;
	cl::Kernel& kernel;
	cl::Context& context;
	cl::Device& device;
	cl::CommandQueue& queue;
};

struct DestegoData {
	int messageLength;
	char* decodedMessage;
	bool read;
	int readChars;
};

int stegoFilter(short* coeffs, tjregion arrayRegion,
    tjregion planeRegion, int componentIndex,
    int transformIndex, tjtransform* transform) {

    StegoData* data = (StegoData*)(transform->data);
    if (!data->written) {
		int msglength = strlen(data->message);
		int arrayLength = arrayRegion.w * arrayRegion.h;

		cl::Buffer buffer_msg(data->context, CL_MEM_READ_WRITE, sizeof(char) * msglength);
		cl::Buffer buffer_coeffs(data->context, CL_MEM_READ_WRITE, sizeof(short) * msglength);

		data->queue.enqueueWriteBuffer(buffer_msg, CL_TRUE, 0, sizeof(char) * msglength, data->message);

		data->kernel.setArg(0, buffer_msg);
		data->kernel.setArg(1, buffer_coeffs);

		data->queue.enqueueNDRangeKernel(data->kernel, cl::NullRange, cl::NDRange(msglength), cl::NullRange);
		data->queue.finish();
		
		data->queue.enqueueReadBuffer(buffer_coeffs, CL_TRUE, 0, sizeof(short) * msglength, coeffs);

		std::cout << "result: ";
		for (int i = 0; i < msglength; i++) {
			std::cout << coeffs[i] << " ";
		}

		data->written = true;
    }

    return 0;
}

int destegoFilter(short* coeffs, tjregion arrayRegion,
    tjregion planeRegion, int componentIndex,
    int transformIndex, tjtransform* transform) {
    DestegoData* data = (DestegoData*)(transform->data);

	if (!data->read) {
		int msglength = data->messageLength;
		int arrayLength = arrayRegion.w * arrayRegion.h;

		int arrayStart = (arrayRegion.y / 8) * arrayRegion.w * arrayRegion.h;



		if (msglength < arrayLength) {
			for (int i = 0; i < msglength; i++) {
				data->decodedMessage[arrayStart + i] = coeffs[i];
			}
		}
		else {
			for (int i = 0; i < arrayLength; i++) {
				data->decodedMessage[arrayStart + i] = coeffs[i];
			}
		}

		if (arrayStart + arrayLength >= msglength) {
			data->read = true;
		}
	}

    return 0;
}

int main() {
	// Get default platform
	std::vector<cl::Platform> all_platforms;
	cl::Platform::get(&all_platforms);

	if (all_platforms.size() == 0) {
		std::cout << " No platforms found. Check OpenCL installation!\n";
		exit(1);
	}

	cl::Platform default_platform = all_platforms[0];
	std::cout << "Using platform: " << default_platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

	// Get default device
	std::vector<cl::Device> all_devices;
	default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);

	if (all_devices.size() == 0) {
		std::cout << " No devices found. Check OpenCL installation!\n";
		exit(1);
	}

	cl::Device default_device = all_devices[0];
	std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << std::endl;

	// Create context
	cl::Context context({ default_device });

	cl::Program::Sources sources;
	std::string kernel_code =
		"	void kernel simple_copy(global const char* A, global short* B) {"
		"		B[get_global_id(0)] = A[get_global_id(0)];"
		"	}";

	sources.push_back({ kernel_code.c_str(), kernel_code.length() });

	cl::Program program(context, sources);
	if (program.build({ default_device }) != CL_SUCCESS) {
		std::cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << std::endl;
		exit(1);
	}

	//cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, sizeof(char) * 10);
	//cl::Buffer buffer_B(context, CL_MEM_READ_WRITE, sizeof(short) * 10);

	//char A[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	cl::CommandQueue queue(context, default_device);
	//queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(char) * 10, A);

	cl::Kernel simple_copy(program, "simple_copy");
	//simple_copy.setArg(0, buffer_A);
	//simple_copy.setArg(1, buffer_B);

	//queue.enqueueNDRangeKernel(simple_copy, cl::NullRange, cl::NDRange(10), cl::NullRange);
	//queue.finish();

	//short B[10];
	//queue.enqueueReadBuffer(buffer_B, CL_TRUE, 0, sizeof(short) * 10, B);
	
	//std::cout << "result: ";
	//for (int i = 0; i < 10; i++) {
	//	std::cout << B[i] << " ";
	//}

	// Test stego filter
	
	char msg[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec ipsum sed nisl consequat vulputate ornare sit amet lorem. Praesent lacinia magna eu odio fermentum commodo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Vivamus ullamcorper dapibus imperdiet. Maecenas sed orci quis sapien pharetra finibus at eu elit. Aenean eget dui molestie, gravida libero eu, dictum leo. Suspendisse potenti. Vivamus faucibus ut nulla pretium tincidunt. Nulla facilisi. Cras sed lobortis nulla. Phasellus sodales odio quis ante congue aliquet. Praesent pulvinar finibus varius. Nunc ac eros euismod, convallis metus ut, imperdiet nunc. Nam a nisi in leo euismod molestie. Etiam nec ultricies massa. Aliquam volutpat cursus porta. Praesent molestie nisl nec rutrum consectetur. Sed fringilla orci eu eros malesuada, sed posuere dui maximus. In ornare ante sit amet mi consectetur malesuada eu quis elit. Nam consectetur rutrum nisi a auctor. Phasellus felis mi, tristique justo."; // 1024 bytes
	
	StegoData data{ msg, false, 0, simple_copy, context, default_device, queue };
	tjtransform xformData{ &data };
	tjregion planeRegion{ 0, 0, 1080, 1064 };
	short coeffs[8640];
	for (int i = 0; i < 8640; i++) {
		coeffs[i] = 0;
	}

	auto start = std::chrono::high_resolution_clock::now();
	for (int component = 0; component < 3; component++) {
		if (component == 0) {
			for (int y = 0; y <= 1064; y += 8) {
				tjregion arrayRegion{ 0, y, 1080, 8 };
				stegoFilter(coeffs, arrayRegion, planeRegion, component, 0, &xformData);
			}
		}
		else {
			for (int y = 0; y <= 528; y += 8) {
				tjregion arrayRegion{ 0, y, 544, 8 };
				stegoFilter(coeffs, arrayRegion, planeRegion, component, 0, &xformData);
			}
		}
	}
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	std::cout << "duration (us): " << duration.count() << std::endl;
	// Test destego filter

	return 0;
}
